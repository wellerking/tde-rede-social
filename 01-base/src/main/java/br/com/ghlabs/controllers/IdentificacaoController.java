package br.com.ghlabs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IdentificacaoController
{
	@RequestMapping("/identificar")
	public String identificar()
	{
		return "usuario-identificar";
	}
}
