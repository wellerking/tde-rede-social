package br.com.ghlabs.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.ProdutoRepository;

@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoRepository repositorio;
	
	@RequestMapping("/produtos")
	public String listar(Model model)
	{
		model.addAttribute("listaProduto", repositorio.obterTodos());
		return "produtos-listar";
	}
	
	@RequestMapping(value="/produtos/novo", method = RequestMethod.GET)
	public String novo(Model model)
	{
		Produto produto;
		produto = new Produto();
		model.addAttribute("produto", produto);
		return "novo-produto";
	}
	
	@RequestMapping(value = "/produtos/novo", method = RequestMethod.POST)
	public String inserir(Produto produto)
	{
		repositorio.inserir(produto);
		System.out.println("Nome enviado: " + produto.getNome());
		System.out.println("Preço enviado: " + produto.getPreco());
		
		return "redirect:/produtos";
	}
}