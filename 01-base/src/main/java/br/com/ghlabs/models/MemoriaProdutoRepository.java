package br.com.ghlabs.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.ghlabs.controllers.Produto;

@Repository
public class MemoriaProdutoRepository implements ProdutoRepository{

	static List<Produto> produtos = new ArrayList<Produto>();
	static {
		
		Produto p1, p2;
		p1 = new Produto()
		{{
			setId(1);
			setNome("iphone");
			setPreco(10_000.0f);
		}};

		p2 = new Produto()
		{{
			setId(2);
			setNome("Nexus");
			setPreco(500.0f);
		}};
		
		produtos.add(p1);
		produtos.add(p2);
	}
	
	@Override
	public List<Produto> obterTodos() 
	{		
		return produtos;
	}
	
	@Override
	public void inserir(Produto produto)
	{
		produtos.add(produto);
	}
}