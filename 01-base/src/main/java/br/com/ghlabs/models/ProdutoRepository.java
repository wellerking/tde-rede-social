package br.com.ghlabs.models;

import java.util.List;

import br.com.ghlabs.controllers.Produto;

public interface ProdutoRepository {
	List<Produto> obterTodos();
	void inserir(Produto produto);
}
