package br.com.bsi.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Musica 
{
	private Integer       id;
	private String        name;
	private List<Artista> artists;
	private Album         album;
	private Calendar      duration;
	private Lyric         lyric;
	
	public Musica(Integer id, String name, Album album, Calendar duration, Lyric letra, List<Artista> artistas)
	{
		artists = new ArrayList<Artista>();
		setId(id);
		setName(name);
		setAlbum(album);
		setDuration(duration);
		setLyric(letra);
		setArtist(artistas);
	}
	
	public Musica(Integer id, String name, Album album, Calendar duration, Lyric letra, Artista artista)
	{
		artists = new ArrayList<Artista>();
		setId(id);
		setName(name);
		setAlbum(album);
		setDuration(duration);
		setLyric(letra);
		addArtista(artista);
	}
	
	public Integer getId() 
	{
		return id;
	}
	
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public List<Artista> getArtist() 
	{
		return artists;
	}
	
	public void setArtist(List<Artista> artist) 
	{
		this.artists = artist;
	}
	
	public void addArtista(Artista artista)
	{
		artists.add(artista);
	}
	
	public Album getAlbum() 
	{
		return album;
	}
	
	public void setAlbum(Album album) 
	{
		this.album = album;
	}
	
	public Calendar getDuration() 
	{
		return duration;
	}
	
	public void setDuration(Calendar duration) 
	{
		this.duration = duration;
	}
	
	public Lyric getLyric() 
	{
		return lyric;
	}
	
	public void setLyric(Lyric lyric) 
	{
		this.lyric = lyric;
	}	
}