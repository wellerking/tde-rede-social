package br.com.bsi.model;

import java.util.List;

public class Artista
{
	private Integer     id;
	private String      nome;
	private List<Album> albuns;
	
	public Artista(Integer id, String nome, List<Album> albuns)
	{
		setNome(nome);
		setAlbuns(albuns);
	}

	public String getNome() 
	{
		return nome;
	}
	
	public void setNome(String nome) 
	{
		this.nome = nome;
	}
	
	public List<Album> getAlbuns() 
	{
		return albuns;
	}
	
	public void setAlbuns(List<Album> albuns) 
	{
		this.albuns = albuns;
	}	
}