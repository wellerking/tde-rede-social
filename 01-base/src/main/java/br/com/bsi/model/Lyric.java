package br.com.bsi.model;

import java.util.ArrayList;
import java.util.List;

public class Lyric 
{
	private Integer      id;
	private List<String> versos;
	
	public Lyric(Integer id, List<String> versos)
	{
		this.versos = new ArrayList<String>();
		setVersos(versos);
	}
	
	public Lyric(Integer id)
	{
		this.versos = new ArrayList<String>();
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public List<String> getVersos()
	{
		return versos;
	}

	public void setVersos(List<String> versos) 
	{
		this.versos = versos;
	}	
	
	public void addVerso(String verso)
	{
		versos.add(verso);
	}
}