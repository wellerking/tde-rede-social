package br.com.bsi.model;

import java.util.List;

public class Album 
{
	private Integer      id;
	private String       nome;
	private List<Musica> musicas;
	
	public Album(Integer id, String nome, List<Musica> musicas)//criar album com as musicas
	{
		setId(id);
		setNome(nome);
		setMusicas(musicas);
	}
	
	public Album(Integer id, String nome)//criar album vazio
	{
		setId(id);
		setNome(nome);
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public List<Musica> getMusicas()
	{
		return musicas;
	}

	public void setMusicas(List<Musica> musicas)//criar o album com todas as musicas ja setadas.
	{
		this.musicas = musicas;
	}	
	
	public void addMusica(Musica musica)//adcionar uma musica de cada vez
	{
		musicas.add(musica);
	}
}